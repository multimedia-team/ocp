#ifndef SIDPLAY_SIDCONFIG_H
#define SIDPLAY_SIDCONFIG_H 1

int __attribute__ ((visibility ("internal"))) sid_config_init (void);
void __attribute__ ((visibility ("internal"))) sid_config_done (void);

#endif
