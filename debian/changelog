ocp (1:0.2.100+ds-2) unstable; urgency=medium

  * Upload to sid.

 -- Gürkan Myczko <tar@debian.org>  Tue, 04 Oct 2022 10:24:42 +0200

ocp (1:0.2.100+ds-1) experimental; urgency=medium

  * New upstream version. (Closes: #1020025)
  * Bump standards version to 4.6.1.
  * d/control: add sensible-utils to Depends.
  * d/clean: updated.
  * d/copyright: add Files-Excluded field.

 -- Gürkan Myczko <tar@debian.org>  Tue, 27 Sep 2022 13:52:10 +0200

ocp (1:0.2.95-1) unstable; urgency=medium

  * New upstream release.

 -- Gürkan Myczko <tar@debian.org>  Mon, 21 Mar 2022 21:46:09 +0100

ocp (1:0.2.94-1) unstable; urgency=medium

  * New upstream release.
  * d/clean: updated.

 -- Gürkan Myczko <tar@debian.org>  Wed, 23 Feb 2022 20:34:08 +0100

ocp (1:0.2.93-2) unstable; urgency=medium

  * d/rules: fix build issues for kfreebsd+hurd.

 -- Gürkan Myczko <tar@debian.org>  Tue, 25 Jan 2022 07:08:31 +0100

ocp (1:0.2.93-1) unstable; urgency=medium

  * New upstream version.
  * d/control: add build-depends libcjson-dev.

 -- Gürkan Myczko <tar@debian.org>  Sun, 23 Jan 2022 13:52:26 +0100

ocp (1:0.2.92+git20220113+ds-2) unstable; urgency=medium

  * d/opencubicplayer.install: fix repeated path segments.
  * d/control: drop unnecessary build-depends.
  * d/watch: update to mangle version.

 -- Gürkan Myczko <tar@debian.org>  Tue, 18 Jan 2022 10:15:44 +0100

ocp (1:0.2.92+git20220113+ds-1) unstable; urgency=medium

  * New upstream version.
  * d/patches: drop some unused ones.
  * d/ocp.1: drop, upstreamed.
  * d/rules: drop some old comments.
  * d/opencubicplayer32.xpm: drop.

 -- Gürkan Myczko <tar@debian.org>  Thu, 13 Jan 2022 12:24:19 +0100

ocp (1:0.2.91-1) unstable; urgency=medium

  * New upstream version.
  * d/control: add libdiscid-dev to Build-Depends.
  * Bump standards version to 4.6.0.
  * Update maintainer address.
  * d/copyright: update years.
  * d/patches: drop some unused ones.

 -- Gürkan Myczko <tar@debian.org>  Sun, 09 Jan 2022 12:49:48 +0100

ocp (1:0.2.90-4) unstable; urgency=medium

  * d/rules: drop the gcc-11/g++-11 exports.
  * Apply patch to fix FTCBFS. Thanks Helmut Grohne. (Closes: #996498)

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Sat, 16 Oct 2021 14:56:48 +0200

ocp (1:0.2.90-3) unstable; urgency=medium

  * Upload to unstable.
  * d/control:
    - drop Build-Depends: gcc-11.
    - update Build-Depends: ttf-unifont to fonts-unifont.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 11 Oct 2021 08:43:22 +0200

ocp (1:0.2.90-2) experimental; urgency=medium

  * d/control: add Build-Depends: gcc-11.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Sat, 10 Apr 2021 19:24:23 +0200

ocp (1:0.2.90-1) experimental; urgency=medium

  * New upstream version. (Closes: #984263)
  * d/watch: bump version, switch to https.
  * Bump standards version to 4.5.1.
  * Bump debhelper version to 13, drop d/compat.
  * d/upstream/metadata: added.
  * d/control:
    - added Rules-Requires-Root.
    - add Build-Depends: libbz2-dev.
  * d/copyright: rewrite to new format.
  * d/clean: added more lines.
  * d/*install: improved.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 15 Mar 2021 21:08:20 +0100

ocp (1:0.2.2+ds-1) unstable; urgency=medium

  * New upstream version. (Closes: #957626)
  * Add ttf-unifont, libjpeg-dev, libpng-dev, libfreetype-dev, xa65 to
    Build-Depends.
  * Bump standards version to 4.5.0.
  * Drop d/rules.old.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 01 Jun 2020 14:54:03 +0200

ocp (1:0.2.0-3) unstable; urgency=medium

  * Team upload
  * Fixup dependency on libbinio-dev

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Mon, 24 Feb 2020 08:54:47 +0100

ocp (1:0.2.0-2) unstable; urgency=medium

  * Team upload.

  [ Gianfranco Costamagna ]
  * Upload to sid.

  [ Gürkan Myczko ]
  * Enable adplug support
  * Require at least libbinio-dev >= 1.5

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Fri, 21 Feb 2020 14:55:55 +0100

ocp (1:0.2.0-1) unstable; urgency=medium

  * New upstream version.
  * Bump standards version to 4.4.1.
  * Update homepage.
  * Swap Maintainer/Uploader fields.
  * Add Vcs fields.
  * d/rules: Refreshed.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Thu, 28 Nov 2019 08:58:19 +0100

ocp (1:0.1.21-5) unstable; urgency=medium

  * Also build on GNU/Hurd, 2nd try.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Wed, 12 Dec 2018 09:09:00 +0100

ocp (1:0.1.21-4) unstable; urgency=medium

  * Also build on GNU/Hurd.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 11 Dec 2018 09:44:06 +0100

ocp (1:0.1.21-3) unstable; urgency=medium

  * Bump standard version to 4.2.1.
  * Bump debhelper version to 11.
  * debian/install: install desktop icon.
  * Improve upstream desktop file.
  * debian/menu: dropped.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 10 Sep 2018 14:30:36 +0200

ocp (1:0.1.21-2) unstable; urgency=medium

  [ Gürkan Myczko ]
  * Really run dh-autoreconf; thanks Logan Rosen. (Closes: #744480)
  * Fix FTBFS on gcc-7; thanks Steve Langasek. (Closes: #853581)
  * Fix FTBFS on i386; thanks Steve Langasek. (Closes: #848916)
  * Bump debhelper version to 10.
  * Bump standards version to 4.1.0.
  * Update my name.

  [ Adam Borowski ]
  * Remove bogus ancient gcc detection completely, fixes FTBFS on gcc-7.
  * Axe redundant/obsolete {Build-,}Depends (dh-autoreconf, autotools-dev,
    texi2html, install-info).
  * wrap-and-sort.
  * Obey dpkg-buildflags.

 -- Adam Borowski <kilobyte@angband.pl>  Mon, 11 Sep 2017 23:29:00 +0200

ocp (1:0.1.21-1.3) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS with dpkg-buildpackage -A, patch by Santiago Vila (Closes:
    #805954).

 -- Andrey Rahmatullin <wrar@debian.org>  Sun, 18 Dec 2016 20:39:25 +0500

ocp (1:0.1.21-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix GCC version detection. Closes: #778033.
  * Build using dh-autoreconf. Closes: #744480.

 -- Matthias Klose <doko@debian.org>  Fri, 10 Jul 2015 14:07:58 +0200

ocp (1:0.1.21-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "FTBFS with gcc-4.7 on i386": add patch fix-gcc47-pcthunk from Kumar
    Appaiah: Fix name of PIC thunks with gcc 4.7.
    Thanks also to Felix Geyer for the bug report and for pointing to the fix.
    (Closes: #683925)

 -- gregor herrmann <gregoa@debian.org>  Sun, 16 Sep 2012 17:19:22 +0200

ocp (1:0.1.21-1) unstable; urgency=low

  * New upstream version.
  * debian/rules: add lintian recommended targets.
  * Bump standards version to 3.9.2.
  * Bump debhelper version to 8.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Wed, 09 Nov 2011 12:30:15 +0100

ocp (1:0.1.20-1) unstable; urgency=low

  [ Gürkan Sengün ]
  * New upstream version. (Closes: #465186)
  * Add info dir. (Closes: #528880)
  * Bump standards version to 3.8.4.
  * debian/control: Add missing misc-depends.
  * Update description.
  * Update debian/copyright.
  * Add desktop-file-utils to build-depends.
  * Add dpkg | install-info to depends.
  * Switch to dpkg-source format version 3 (quilt).
  * Drop debian/overrides.

  [ Axel Beckert ]
  * debian/rules: Remove usr/share/info/dir.gz before building package.
  * debian/rules: Add man page symlinks ocp-vcsa, ocp-curses and ocp-x11.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Wed, 19 May 2010 12:05:23 +0200

ocp (1:0.1.17-2) unstable; urgency=low

  * Don't install the info dir files. (Closes: #537835)
    Thanks Santiago Vila for the patch.
  * Bump standards version.
  * Updated debian/copyright.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Mon, 31 Aug 2009 14:25:19 +0200

ocp (1:0.1.17-1) unstable; urgency=low

  * New upstream version. (Closes: #535005, #453877)
  * Add libasound2-dev for ALSA support. (Closes: #453871)
  * Updated homepage address.
  * Update my email address.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Mon, 29 Jun 2009 12:01:39 +0200

ocp (1:0.1.15-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * FTBFS: oplplay.cpp:366: error: 'memcpy' was not declared in this
    scope (Closes: #474834)

 -- Chris Lamb <chris@chris-lamb.co.uk>  Sat, 12 Apr 2008 00:39:28 +0100

ocp (1:0.1.15-1) unstable; urgency=low

  * New upstream version.
  * debian/control:
    + Moved homepage field.
    + Updated standards version.
    + Added provides line.
  * debian/watch: Added.
  * debian/compat: Bumped version.
  * debian/copyright: Updated.
  * Added Multimedia Team to Uploaders.
  * Added an override for an empty directory that is needed.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Tue, 15 Jan 2008 11:52:34 +0100

ocp (1:0.1.14-1) unstable; urgency=low

  * New upstream version.
  * Removed wrong leftover line from manual page debian/pak.1.
  * Updated debian/control descriptions to reflect flac and adlib playing.
  * Added libflac-dev to debian/control.
  * Applied patch from Martin Michlmayr to fix FTBFS with GCC 4.3.
    (Closes: #417455)
  * Fixed desktop file. (Closes: #444440)

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Mon, 22 Oct 2007 17:16:01 +0200

ocp (1:0.1.13-2) unstable; urgency=low

  * Fix the section for the documentation package.
  * Install the Quake pak file generator.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Tue,  6 Feb 2007 22:08:44 +0100

ocp (1:0.1.13-1) unstable; urgency=low

  * New upstream version.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Mon,  5 Feb 2007 16:09:25 +0100

ocp (1:0.1.12-1) unstable; urgency=low

  * New upstream version.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Mon, 25 Dec 2006 18:27:16 +0100

ocp (1:0.1.11-1) unstable; urgency=low

  * New upstream version.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Thu, 14 Dec 2006 11:12:01 +0100

ocp (1:0.1.10-1) unstable; urgency=low

  * New upstream version.
  * Build on all architectures.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Tue, 12 Dec 2006 12:59:28 +0100

ocp (0.1.10rc6-3) unstable; urgency=low

  * Rebuild against latest libadplug-dev.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Sun, 17 Sep 2006 19:02:10 +0200

ocp (0.1.10rc6-2) unstable; urgency=low

  * Fix path in .desktop file. (Closes: #383577)
  * Run in terminal from .desktop. (Closes: #383578)

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Fri, 18 Aug 2006 08:25:30 +0200

ocp (0.1.10rc6-1) unstable; urgency=high

  * New upstream version:
    - fixes multiple buffer overflows, namely those in
      playgmd/gmdls3m.cpp, playit/itload.cpp, playgmd/gmdlams.cpp,
      playgmd/gmdlult.cpp, and (Closes: #381098)
  * Updated URLs.
  * Bump standards version.
  * Added missing libadplug-dev to build-depends, and libadplug0c2a to depends.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Thu,  3 Aug 2006 22:39:04 +0200

ocp (0.1.10rc5-1) unstable; urgency=low

  * New upstream version. (Closes: #343649, #343650)
  * Update build-depends. (closes: #346592)

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Wed, 5 Apr 2006 14:43:11 +0100

ocp (0.1.9-2.2) unstable; urgency=medium

  * Non-maintainer upload, adding libxext-dev, x-dev to not rely
    on indirect dependencies.

 -- Thomas Viehmann <tv@beamnet.de>  Sat, 14 Jan 2006 18:32:04 +0100

ocp (0.1.9-2.1) unstable; urgency=medium

  * NMU for x-window-system-dev/xlibs-dev removal. Closes: #346592.
    Thanks to Justin Pryzby for calculating the necessary change.

 -- Thomas Viehmann <tv@beamnet.de>  Sat, 14 Jan 2006 09:58:30 +0100

ocp (0.1.9-2) unstable; urgency=low

  * Fix build-depends for doc, add texinfo.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Sun, 27 Nov 2005 13:19:05 +0100

ocp (0.1.9-1) unstable; urgency=low

  * Initial Release. (closes: #301094)
  * Setup sane ocp.ini.
  * Removed cpiface/gif.*

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Mon, 2 May 2005 15:49:48 +0100
