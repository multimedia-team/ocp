#ifndef PLAYTIMIDITY_TIMIDITYCONFIG_H
#define PLAYTIMIDITY_TIMIDITYCONFIG_H 1

int __attribute__ ((visibility ("internal"))) timidity_config_init (void);

void __attribute__ ((visibility ("internal"))) timidity_config_done (void);

#endif
